///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/27/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
#include "fish.hpp"

using namespace std;

namespace animalfarm{
   class Nunu : public Fish {
      public:
         bool isNative;
         Nunu (bool Native, enum Color newColor, enum Gender newGender);

         void printInfo();
   };
}
