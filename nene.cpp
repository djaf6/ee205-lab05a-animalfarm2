///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/27/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include "nene.hpp"

using namespace std;

namespace animalfarm {
   Nene::Nene (string newTagID, enum Color newColor, enum Gender newGender){
      tagID = newTagID;
      species = "Branta sandvicensis";
      gender = newGender;
      featherColor = newColor;
      isMigratory = true;
   }

   const string Nene::speak(){
      return "Nay, nay";
   }

   void Nene::printInfo(){
      cout << "Nene" << endl;
      cout << "   Tag ID = [" << tagID << "]" << endl;
      Bird::printInfo();
   }
}
